import datetime
import time
import usb

class BattInfo:
    def __init__(self):
        self.level = 0
        self.charging = False
        self.extra = 0

    def parse_from(self, data):
        data_len = len(data)
        self.level = data[6] if data_len > 7 else 0
        self.charging = data[7] != 0 if data_len > 8 else False
        self.extra = data[16] if data_len > 16 else -1

    def __repr__(self):
        status = "charging" if self.charging else "discharging"
        return f"{self.level*10}%, {status}, last byte: {self.extra}"

class DevInfo:
    def __init__(self, vid, pid, dev, iface, ep):
        self.vid = vid
        self.pid = pid
        self.dev_num = dev
        self.iface_num = iface
        self.ep_num = ep

class Worker:
    def __init__(self):
        self.usb_dev_info = DevInfo(0x25a7, 0xfa09, 0, 1, 0)
        #self.devInfo = DevInfo(0x25a7, 0xfa0a, 0, 1, 0) # wired

        self.usb_dev = usb.core.find(idVendor = self.usb_dev_info.vid, idProduct = self.usb_dev_info.pid)
        self.usb_iface = self.usb_dev[self.usb_dev_info.dev_num].interfaces()[self.usb_dev_info.iface_num]
        self.usb_ep = self.usb_iface.endpoints()[self.usb_dev_info.ep_num]

        self.acquire_device()

    def acquire_device(self):
        print("\nACQUIRING DEVICE\n")
        for iface in range(len(self.usb_dev[self.usb_dev_info.dev_num].interfaces())):
            if self.usb_dev.is_kernel_driver_active(iface):
                self.usb_dev.detach_kernel_driver(iface)

    def free_device(self):
        print("\nRELEASING DEVICE\n")
        try:
            for iface in range(len(self.usb_dev[self.usb_dev_info.dev_num].interfaces())):
                self.usb_dev.attach_kernel_driver(iface)
        except usb.core.USBError as e:
            if e.errno != 16: # Resource busy
                print(e)

    def request_battery_info(self):
       info = BattInfo()

       try:
           request = [0x08, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x49]
           print("TX:\t", request)
           self.usb_dev.ctrl_transfer(
                   0x21,  # REQUEST_TYPE_CLASS | RECIPIENT_INTERFACE | ENDPOINT_OUT
                   9,     # SET_REPORT
                   0x0308, # reportType: x03, reportId: x08
                   self.usb_dev_info.iface_num,
                   request
               )
       except usb.USBError as e:
           print(e)
           return info

       try:
           data = self.usb_dev.read(self.usb_ep.bEndpointAddress, self.usb_ep.wMaxPacketSize)
           print('RX:\t',data)
           info.parse_from(data)
       except usb.USBError as e:
           print(e)

       return info

def main():
    worker = Worker()
    try:
        while True:
            print(datetime.datetime.now().strftime("%H:%M:%S:"))
            print("inf: ", worker.request_battery_info(), "\n")
            time.sleep(1)
    finally:
        # doesn’t matter if it an emergency or normal exit,
        # try reloading the driver:
        worker.free_device()

if __name__=='__main__':
    main()
