BattMon — a USB bridge to display Redragon mouse battery level:

![](./misc/readme_img/mouse.gif)

Based on:

USB Host Shield mini:

![](./misc/readme_img/UHSM_prod.jpg)

Lilygo T-Display-S3:

![](./misc/readme_img/t-display-s3.jpg)

See also README in [./lib](./lib/README) and the related blog post: TODO
