#pragma once

#include "Arduino.h"
#include "batt/batteryinfohandlerbase.h"
#include "esp_lcd_panel_io.h"
#include "esp_lcd_panel_ops.h"
#include "esp_lcd_panel_vendor.h"
#include "lvgl.h"
#include "pin_config.h"

class Display
{
public:
    enum Tiles
    {
        // TClock = 0,
        // TLogo,
        // TTechInfo,
        TBattMon = 0,

        LastTile = TBattMon
    };

    Display();
    ~Display();

    void setup();

    void loop();

    void createUI();

    void inf(const String &txt);
    void wrn(const String &txt);
    void updateBatteryInfo(const BatteryInfoHandlerBase::BattInfo &info, const bool force = false);

    static const BatteryInfoHandlerBase::BattInfo *const getBattInfo();

private:
    esp_lcd_panel_io_handle_t io_handle;
    lv_disp_draw_buf_t disp_buf; // contains internal graphic buffer(s) called draw buffer(s)
    lv_disp_drv_t disp_drv; // contains callback functions
    lv_color_t *lv_disp_buf;

    lv_obj_t *m_dis;

    BatteryInfoHandlerBase::BattInfo m_battInfo;

    lv_obj_t *m_lblBattImage { nullptr };
    lv_obj_t *m_lblBattStatus { nullptr };
    lv_obj_t *m_lblBattInfo { nullptr };

    lv_timer_t *m_blinkTimer { nullptr };

    lv_obj_t *createTile(uint8_t num);
    void createPage_BattMon();

    enum Orientation
    {
        LeftOfHost = 0,
        RightOfHost
    };
    void setOrientation(Orientation orient);
};