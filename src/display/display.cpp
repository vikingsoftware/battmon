#include "display.h"

#include "app/battmonlog.h"
#include "batt/batterytimeestimator.h"

LV_IMG_DECLARE(batt_0);
LV_IMG_DECLARE(batt_10);
LV_IMG_DECLARE(batt_20);
LV_IMG_DECLARE(batt_30);
LV_IMG_DECLARE(batt_40);
LV_IMG_DECLARE(batt_50);
LV_IMG_DECLARE(batt_60);
LV_IMG_DECLARE(batt_70);
LV_IMG_DECLARE(batt_80);
LV_IMG_DECLARE(batt_90);
LV_IMG_DECLARE(batt_100);
static const lv_img_dsc_t *battImages[11] = { &batt_0,  &batt_10, &batt_20, &batt_30, &batt_40, &batt_50,
                                              &batt_60, &batt_70, &batt_80, &batt_90, &batt_100 };

#define UI_BG_COLOR lv_color_black()
#define UI_FRAME_COLOR lv_color_hex(0x282828)
#define UI_FONT_COLOR lv_color_white()

#define LV_DELAY(x)                                                                                                    \
    do {                                                                                                               \
        uint32_t t = x;                                                                                                \
        while (t--) {                                                                                                  \
            lv_timer_handler();                                                                                        \
            delay(1);                                                                                                  \
        }                                                                                                              \
    } while (0);

static bool is_initialized_lvgl { false };

Display::Display()
    : io_handle(nullptr)
{
    BattMonLog::TraceMe("DisplayTest");
}

Display::~Display()
{
    BattMonLog::TraceMe("~DisplayTest");
}

/* static */ bool notify_display_flush_ready(esp_lcd_panel_io_handle_t panel_io, esp_lcd_panel_io_event_data_t *edata,
                                             void *user_ctx)
{
    if (is_initialized_lvgl) {
        lv_disp_drv_t *disp_driver = (lv_disp_drv_t *)user_ctx;
        lv_disp_flush_ready(disp_driver);
    }
    return false;
}

/* static */ void display_flush(lv_disp_drv_t *drv, const lv_area_t *area, lv_color_t *color_map)
{
    esp_lcd_panel_handle_t panel_handle = (esp_lcd_panel_handle_t)drv->user_data;
    int offsetx1 = area->x1;
    int offsetx2 = area->x2;
    int offsety1 = area->y1;
    int offsety2 = area->y2;
    // copy a buffer's content to a specific area of the display
    esp_lcd_panel_draw_bitmap(panel_handle, offsetx1, offsety1, offsetx2 + 1, offsety2 + 1, color_map);
}

void Display::setup()
{
    BattMonLog::TraceMe("setup");

    pinMode(PIN_LCD_RD, OUTPUT);
    digitalWrite(PIN_LCD_RD, HIGH);
    esp_lcd_i80_bus_handle_t i80_bus = NULL;
    esp_lcd_i80_bus_config_t bus_config = {
        .dc_gpio_num = PIN_LCD_DC,
        .wr_gpio_num = PIN_LCD_WR,
        .clk_src = LCD_CLK_SRC_PLL160M,
        .data_gpio_nums =
            {
                PIN_LCD_D0,
                PIN_LCD_D1,
                PIN_LCD_D2,
                PIN_LCD_D3,
                PIN_LCD_D4,
                PIN_LCD_D5,
                PIN_LCD_D6,
                PIN_LCD_D7,
            },
        .bus_width = 8,
        .max_transfer_bytes = LVGL_LCD_BUF_SIZE * sizeof(uint16_t),
    };
    esp_lcd_new_i80_bus(&bus_config, &i80_bus);

    esp_lcd_panel_io_i80_config_t io_config = {
        .cs_gpio_num = PIN_LCD_CS,
        .pclk_hz = ST7789_LCD_PIXEL_CLOCK_HZ,
        .trans_queue_depth = 20,
        .on_color_trans_done = notify_display_flush_ready,
        .user_ctx = &disp_drv,
        .lcd_cmd_bits = 8,
        .lcd_param_bits = 8,
        .dc_levels =
            {
                .dc_idle_level = 0,
                .dc_cmd_level = 0,
                .dc_dummy_level = 0,
                .dc_data_level = 1,
            },
    };
    ESP_ERROR_CHECK(esp_lcd_new_panel_io_i80(i80_bus, &io_config, &io_handle));
    esp_lcd_panel_handle_t panel_handle = NULL;
    esp_lcd_panel_dev_config_t panel_config = {
        .reset_gpio_num = PIN_LCD_RES,
        .color_space = ESP_LCD_COLOR_SPACE_RGB,
        .bits_per_pixel = 16,
    };
    esp_lcd_new_panel_st7789(io_handle, &panel_config, &panel_handle);
    esp_lcd_panel_reset(panel_handle);
    esp_lcd_panel_init(panel_handle);
    esp_lcd_panel_invert_color(panel_handle, true);

    esp_lcd_panel_set_gap(panel_handle, 0, 35);
    esp_lcd_panel_swap_xy(panel_handle, true);

    lv_init();
    lv_disp_buf = (lv_color_t *)heap_caps_malloc(LVGL_LCD_BUF_SIZE * sizeof(lv_color_t),
                                                 MALLOC_CAP_DMA | MALLOC_CAP_INTERNAL);

    lv_disp_draw_buf_init(&disp_buf, lv_disp_buf, NULL, LVGL_LCD_BUF_SIZE);

    lv_disp_drv_init(&disp_drv);

    disp_drv.hor_res = ST7789_LCD_H_RES;
    disp_drv.ver_res = ST7789_LCD_V_RES;
    disp_drv.flush_cb = display_flush;
    disp_drv.draw_buf = &disp_buf;
    disp_drv.user_data = panel_handle;

    lv_disp_drv_register(&disp_drv);

    is_initialized_lvgl = true;

    /* Lighten the screen with gradient */
    ledcSetup(0, 10000, 8);
    ledcAttachPin(PIN_LCD_BL, 0);

    setOrientation(Orientation::LeftOfHost);

    createUI();
}

void Display::setOrientation(Orientation orient)
{
    // swithc ligting off
    ledcWrite(0, 0);

    bool mirrorX, mirrorY;

    switch (orient) {
    case Orientation::LeftOfHost: {
        mirrorX = true;
        mirrorY = false;
        break;
    }
    case Orientation::RightOfHost: {
        mirrorX = false;
        mirrorY = true;
        break;
    }
    }

    esp_lcd_panel_handle_t panel_handle = (esp_lcd_panel_handle_t)disp_drv.user_data;
    esp_lcd_panel_mirror(panel_handle, mirrorX, mirrorY);

    // switch the lightning on:
    for (uint8_t i = 0; i < 0xFF; i++) {
        ledcWrite(0, i);
        delay(5);
    }
}

void Display::loop()
{
    // BattMonLog::TraceMe("loop");
    lv_timer_handler();
}

lv_obj_t *Display::createTile(uint8_t num)
{
    BattMonLog::TraceMe("createTile");

    return lv_tileview_add_tile(m_dis, 0, num, LV_DIR_HOR);
}

void Display::createUI()
{
    BattMonLog::TraceMe("createUI");

    m_dis = lv_tileview_create(lv_scr_act());
    lv_obj_align(m_dis, LV_ALIGN_TOP_RIGHT, 0, 0);
    lv_obj_set_size(m_dis, LV_PCT(100), LV_PCT(100));

    createPage_BattMon();

    lv_obj_t *pgBatt = lv_tileview_add_tile(m_dis, 0, 1, LV_DIR_HOR);
}

void Display::createPage_BattMon()
{
    BattMonLog::TraceMe("createPage_BattMon");

    lv_obj_t *tile = createTile(Tiles::TBattMon);
    m_lblBattImage = lv_img_create(tile);
    lv_img_set_src(m_lblBattImage, &batt_0);
    lv_obj_center(m_lblBattImage);

    m_lblBattStatus = lv_label_create(tile);
    lv_obj_align(m_lblBattStatus, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_text_font(m_lblBattStatus, &lv_font_montserrat_48, 0);
    lv_label_set_text(m_lblBattStatus, "");
    lv_obj_set_style_text_color(m_lblBattStatus, UI_FONT_COLOR, 0);
    lv_obj_set_style_text_align(m_lblBattStatus, LV_ALIGN_CENTER, 0);

    m_lblBattInfo = lv_label_create(tile);
    lv_obj_align(m_lblBattInfo, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_text_font(m_lblBattInfo, &lv_font_montserrat_18, 0);
    lv_label_set_text(m_lblBattInfo, "No mouse");
    lv_obj_set_style_text_color(m_lblBattInfo, UI_FONT_COLOR, 0);
    lv_obj_align_to(m_lblBattInfo, m_lblBattStatus, LV_ALIGN_OUT_BOTTOM_MID, 0, 0);

    // m_lblNoMouse = lv_label_create(tile);
    // lv_obj_align(m_lblNoMouse, LV_ALIGN_TOP_LEFT, 0, 0);
    // lv_obj_set_style_text_font(m_lblNoMouse, &lv_font_montserrat_18, 0);
    // lv_label_set_text(m_lblNoMouse, LV_SYMBOL_USB "No mouse");
    // lv_obj_set_style_text_color(m_lblNoMouse, UI_FONT_COLOR, 0);
    // lv_obj_set_style_text_align(m_lblNoMouse, LV_ALIGN_CENTER, 0);

    // m_lblCharging = lv_label_create(tile);
    // lv_obj_align(m_lblCharging, LV_ALIGN_TOP_RIGHT, 0, 0);
    // lv_obj_set_style_text_font(m_lblCharging, &lv_font_montserrat_18, 0);
    // lv_label_set_text(m_lblCharging, LV_SYMBOL_CHARGE);
    // lv_obj_set_style_text_color(m_lblCharging, UI_FONT_COLOR, 0);
    // lv_obj_set_style_text_align(m_lblCharging, LV_ALIGN_CENTER, 0);

    // lv_timer_create(blinkLabel2, 1000, m_lblCharging);
    //  lv_timer_create(blinkLabel1, 1000, m_lblNoMouse);
}

void Display::inf(const String &txt)
{
    BattMonLog::log(txt);
}

void Display::wrn(const String &txt)
{
    BattMonLog::log(txt);
}

static void blinkLabel(lv_timer_t *t)
{
    if (lv_obj_t *lbl = (lv_obj_t *)t->user_data) {
        if (lv_obj_has_flag(lbl, LV_OBJ_FLAG_HIDDEN))
            lv_obj_clear_flag(lbl, LV_OBJ_FLAG_HIDDEN);
        else
            lv_obj_add_flag(lbl, LV_OBJ_FLAG_HIDDEN);
    }
}

static void setLabelText(lv_obj_t *lbl, const String &txt)
{
    const int len = txt.length() + 1;
    char buf[len];
    txt.toCharArray(buf, len);
    lv_label_set_text(lbl, buf);
}

void Display::updateBatteryInfo(const BatteryInfoHandlerBase::BattInfo &info, const bool force)
{
    const bool changed = info != m_battInfo;
    BattMonLog::log("New BI: " + info.toString());
    if (!changed && !force)
        return;
    BattMonLog::log("Old BI: " + m_battInfo.toString());

    String statusText, infoText;
    if (!info.plugged) {
        statusText = String(LV_SYMBOL_USB);
        infoText = String("No mouse");
    } else {
        statusText = String(info.levelPercent, DEC);
        switch (info.state) {
        case BatteryInfoHandlerBase::BattState::Charged: {
            statusText += String(LV_SYMBOL_CHARGE);
            infoText = String("Charged");
            break;
        }
        case BatteryInfoHandlerBase::BattState::Charging: {
            statusText += String(LV_SYMBOL_CHARGE);
            infoText = String("Charging");
            break;
        }
        case BatteryInfoHandlerBase::BattState::Draining: {
            statusText += String("%");
            infoText = String("Draining");
            break;
        }
        default:
            break;
        }
    }

    const uint8_t delta = abs(m_battInfo.level - info.level);
    if (delta > 0) {
        uint8_t pos = info.level;
        if (pos < 0)
            pos = 0;
        if (pos > 11)
            pos = 11;

        lv_img_set_src(m_lblBattImage, battImages[pos]);
    }

    // infoText = infoText + BatteryTimeEstimator::updateEstimation(info);

    setLabelText(m_lblBattStatus, statusText);
    lv_obj_align_to(m_lblBattStatus, lv_scr_act(), LV_ALIGN_CENTER, 0, 0);
    setLabelText(m_lblBattInfo, infoText);
    lv_obj_align_to(m_lblBattInfo, m_lblBattStatus, LV_ALIGN_OUT_BOTTOM_MID, 0, 0);

    const bool needBlink = !info.plugged || info.state == BatteryInfoHandlerBase::BattState::Charging;
    if (needBlink && !m_blinkTimer) {
        m_blinkTimer = lv_timer_create(blinkLabel, 1000, m_lblBattStatus);
    } else if (!needBlink && m_blinkTimer) {
        lv_timer_del(m_blinkTimer);
        m_blinkTimer = nullptr;
        if (lv_obj_has_flag(m_lblBattStatus, LV_OBJ_FLAG_HIDDEN))
            lv_obj_clear_flag(m_lblBattStatus, LV_OBJ_FLAG_HIDDEN);
    }

    m_battInfo = info;
}

// bool DisplayTest::handleMousePlugged(const BatteryInfoHandlerBase::BattInfo &info)
// {
//     bool changed = false;
//     if (m_lblNoMouse)
//     {
//         changed = m_battInfo.plugged != info.plugged;
//         if (!info.plugged && !m_pluggedBlinkTimer)
//         {
//             BattMonLog::log("m_lblNoMouse: ");
//             BattMonLog::log((ptrdiff_t)m_lblNoMouse, HEX);
//             BattMonLog::log("starting timer");

//             m_pluggedBlinkTimer = lv_timer_create(blinkLabel, 1000, m_lblNoMouse);
//         }
//         else if (info.plugged && m_pluggedBlinkTimer)
//         {
//             BattMonLog::log("m_lblNoMouse: ");
//             BattMonLog::log((ptrdiff_t)m_lblNoMouse, HEX);
//             BattMonLog::log("stoping timer");

//             lv_timer_del(m_pluggedBlinkTimer);
//             m_pluggedBlinkTimer = nullptr;
//             if (!lv_obj_has_flag(m_lblNoMouse, LV_OBJ_FLAG_HIDDEN))
//                 lv_obj_add_flag(m_lblNoMouse, LV_OBJ_FLAG_HIDDEN);
//         }
//     }
//     return changed;
// }

// bool DisplayTest::handleChargingStateChanged(const BatteryInfoHandlerBase::BattInfo &info)
// {
//     bool changed = false;
//     if (m_lblCharging)
//     {
//         changed = m_battInfo.state != info.state;
//         if (info.state != BatteryInfoHandlerBase::BattState::Charging)
//         {
//             if (m_chargingBlinkTimer)
//             {
//                 lv_timer_del(m_chargingBlinkTimer);
//                 m_chargingBlinkTimer = nullptr;
//             }
//         }

//         if (info.state == BatteryInfoHandlerBase::BattState::Charging)
//         {
//             if (!m_chargingBlinkTimer)
//                 m_chargingBlinkTimer = lv_timer_create(blinkLabel, 1000, m_lblCharging);
//         }

//         if (info.state == BatteryInfoHandlerBase::BattState::Draining)
//         {
//             if (!lv_obj_has_flag(m_lblCharging, LV_OBJ_FLAG_HIDDEN))
//                 lv_obj_add_flag(m_lblCharging, LV_OBJ_FLAG_HIDDEN);
//         }
//     }
//     return changed;
// }
