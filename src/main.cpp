#define DEBUG_USB_HOST

#include "app/battmonapp.h"
#include "app/battmonlog.h"
#include "usb/device/devicelistener.h"
#include "usb/host/hostlistener.h"
#include "usb/usbbridge.h"

#include <Arduino.h>
#include <Usb.h>

UHS_USB deviceUsb;
DeviceListener deviceListener(&deviceUsb);
HostListener hostListener;
BattMonApp *pApp = nullptr;

void setup()
{
    BattMonLog::init();
    pApp = BattMonApp::create();
    pApp->init(&deviceListener, &hostListener);
}

void loop()
{
    pApp->processEvents();
}