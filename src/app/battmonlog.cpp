#include "app/battmonlog.h"

/* static */ BattMonLog *BattMonLog::m_instance = nullptr;

/* static */ BattMonLog *BattMonLog::instance()
{
    if (!m_instance)
        m_instance = new BattMonLog();
    return m_instance;
}

BattMonLog::BattMonLog() { }

void BattMonLog::init()
{
    Serial.begin(115200);
    delay(500);

    Serial.println("Before ble");

    // instance()->_ble.begin("BattMonSerialBLE");

    delay(500);
    Serial.println("BLE started");
}

void BattMonLog::log(const String &txt)
{

    if (Serial) {
        Serial.println(txt);
    }
}

/* static */ void BattMonLog::hexes(const uint8_t *data, const uint16_t len)
{
    String s("HEX data [" + String(len) + "]:");
    for (int i = 0; i < len; ++i)
        s += ", 0x" + String(data[i], HEX);
    BattMonLog::log(s);
}