#pragma once

#include <Arduino.h>

class BattMonLog
{
public:
    struct TraceMe {
        TraceMe(const String &msg)
            : m_msg(msg)
        {
            log("<" + m_msg + ">");
        }
        ~TraceMe() { log("</" + m_msg + ">"); }
        String m_msg;
    };
    static void init();
    static void log(const String &txt);
    static void hexes(const uint8_t *data, const uint16_t len);

private:
    BattMonLog();
    static BattMonLog *m_instance;
    static BattMonLog *instance();
};
