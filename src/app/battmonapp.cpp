#include "app/battmonapp.h"

#include "app/battmonlog.h"
#include "display/display.h"
#include "usb/device/devicelistener.h"
#include "usb/host/hostlistener.h"
#include "usb/usbbridge.h"

#include <Arduino.h>
#include <hidescriptorparser.h>

/*static*/ BattMonApp *BattMonApp::m_instance = nullptr;
/*static*/ const uint8_t BattMonApp::PinBtnBoot = 0;
/*static*/ const uint8_t BattMonApp::PinBtnOther = 14;

/*static*/ BattMonApp *BattMonApp::create()
{
    return m_instance ? m_instance : m_instance = new BattMonApp();
}

/*static*/ BattMonApp *BattMonApp::instance()
{
    return m_instance;
}

BattMonApp::BattMonApp()
    : m_display(new Display)
{
}

BattMonApp::~BattMonApp()
{
    delete m_display;
    m_display = nullptr;
}

bool BattMonApp::init(DeviceListener *pDevice, HostListener *pHost)
{
    pinMode(PinBtnBoot, INPUT_PULLUP);
    pinMode(PinBtnOther, INPUT_PULLUP);

    if (!UsbBridge::init(pDevice, pHost)) {
        BattMonLog::log("Can't init external USB stack.");
        return false;
    }

    m_display->setup();

    m_display->updateBatteryInfo(m_lastBattInfo, true);

    m_ready = true;
    return true;
}

void BattMonApp::processEvents()
{
    UsbBridge::processEvents();
    m_display->loop();

    readButtonPins();

    static const uint16_t interval = 1000;
    const unsigned long now = millis();
    static unsigned long lastRequestTime = now;

    if (now - lastRequestTime >= interval) {
        lastRequestTime = now;
        tickOneSecond();
    }
}

void BattMonApp::tickOneSecond()
{
    updateBattInfo();
}

void BattMonApp::readButtonPins()
{
    // -1 Pressed
    // 0 Not changed
    // 1 Released
    auto readButtonPin = [](int pin, int &holder) -> int {
        const int btnVal = digitalRead(pin);
        if (btnVal != holder) {
            holder = btnVal;
            return holder == LOW ? -1 : 1;
        }
        return 0;
    };

    auto handleBtn = [&readButtonPin, this](int pin, int &holder) {
        switch (readButtonPin(pin, holder)) {
        case -1: {
            onBtnPressed(pin);
            break;
        }
        case 1: {
            onBtnReleased(pin);
            break;
        }
        default:
            break;
        }
    };

    handleBtn(PinBtnBoot, m_btnStateBoot);
    handleBtn(PinBtnOther, m_btnStateOther);
}

void BattMonApp::onBtnPressed(int btnPin)
{
    switch (btnPin) {
    case PinBtnOther: {
        BattMonLog::log("Non Boot Btn Pressed");
        if (UsbBridge::isMousePluggedIn())
            UsbBridge::getDevice()->requestBatLevel();
        break;
    }
    default: {
        BattMonLog::log("Boot Btn Pressed");
        break;
    }
    }
}

void BattMonApp::onBtnReleased(int btnPin)
{
    switch (btnPin) {
    case PinBtnOther: {
        BattMonLog::log("Non Boot Btn Released");
        break;
    }
    default: {
        BattMonLog::log("Boot Btn Released");
        break;
    }
    }
}

void BattMonApp::updateBattInfo()
{
    const bool mousePresent = UsbBridge::isMousePluggedIn();
    BatteryInfoHandlerBase::BattInfo info =
            mousePresent ? UsbBridge::getDevice()->getBattInfo() : BatteryInfoHandlerBase::BattInfo();
    info.plugged = mousePresent;
    if (!info.plugged)
        info.levelPercent = 0;

    if (info != m_lastBattInfo) {
        BattMonLog::log("old: " + m_lastBattInfo.toString());
        BattMonLog::log("new: " + info.toString());

        m_lastBattInfo = info;
        m_display->updateBatteryInfo(m_lastBattInfo);
    }

    if (mousePresent) {
        static const unsigned long reqIntervalMs = 10000;
        static unsigned long lastReqTime = 0;
        const unsigned long now = millis();
        if (now - reqIntervalMs > lastReqTime) {
            lastReqTime = now;
            UsbBridge::getDevice()->requestBatLevel();
        }
    }
}
