#pragma once

#include "batt/batteryinfohandlerbase.h"

class Display;
class DeviceListener;
class HostListener;
class UHS_USB;

class BattMonApp
{
public:
    static BattMonApp *create();
    static BattMonApp *instance();

    ~BattMonApp();

    bool init(DeviceListener *pDevice, HostListener *pHost);

    void processEvents();

    Display *getDisplay() { return m_display; }

private:
    BattMonApp();

    static BattMonApp *m_instance;
    static const uint8_t PinBtnBoot;
    static const uint8_t PinBtnOther;

    Display *m_display { nullptr };

    int m_btnStateBoot { HIGH };
    int m_btnStateOther { HIGH };

    bool m_ready { false };

    BatteryInfoHandlerBase::BattInfo m_lastBattInfo;

    void readButtonPins();

    void onBtnPressed(int btnPin);
    void onBtnReleased(int btnPin);

    void updateBattInfo();

    void tickOneSecond();
};