#include "usb/device/uhshiddescrparser.h"

#include "app/battmonlog.h"

UHSHIDDescrParser::UHSHIDDescrParser()
    : USBReadParser()
{
}

UHSHIDDescrParser::~UHSHIDDescrParser()
{
    m_descr.reset();
}

void UHSHIDDescrParser::Parse(const uint16_t len, const uint8_t *pbuf, const uint16_t &offset)
{
    m_descr.addRawDescrPart(pbuf, len);

    // String msg = String("Descr: ");
    // for(int i=0; i < m_descr.length; ++i)
    //     msg += " 0x" + String(m_descr.data[i], HEX);
    // BattMonLog::log("UHSHIDDescrParser::Parse, len: " +String(len) +", offset: " + String(offset) + "\n" + msg);
}
