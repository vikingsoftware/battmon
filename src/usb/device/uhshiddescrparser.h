#pragma once

#include "usb/battmonhiddescr.h"

#include <hidescriptorparser.h>

class UHSHIDDescrParser : public USBReadParser
{
public:
    UHSHIDDescrParser();
    ~UHSHIDDescrParser();

    void Parse(const uint16_t len, const uint8_t *pbuf, const uint16_t &offset) override;

    BattMonHidDescr m_descr;
};