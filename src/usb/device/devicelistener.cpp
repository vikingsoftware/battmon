#include "usb/device/devicelistener.h"

#include "app/battmonapp.h"
#include "app/battmonlog.h"
#include "batt/battinfohandlerfactory.h"
#include "usb/device/uhshiddescrparser.h"
#include "usb/usbbridge.h"

#include <Arduino.h>
#include <Usb.h>

DeviceListener::DeviceListener(UHS_USB *pUsb)
    : HIDComposite(pUsb)
    , m_pUsb(pUsb)
{
}

DeviceListener::~DeviceListener() { }

uint8_t DeviceListener::GetReportDescr(uint16_t wIndex, USBReadParser *parser)
{
    const uint8_t constBufLen = 128;
    uint8_t buf[constBufLen];

    return pUsb->ctrlReq(bAddress, 0x00, bmREQ_HID_REPORT, USB_REQUEST_GET_DESCRIPTOR, 0x00, HID_DESCRIPTOR_REPORT,
                         wIndex, 256, constBufLen, buf, parser);
}

bool DeviceListener::SelectInterface(uint8_t iface, uint8_t proto)
{
    BattMonLog::log(String("SelectInterface, iface: ") + String(iface) + String(", proto: ") + String(proto));

    m_ifaceProtos[iface] = proto;

    return true; // accept all ifaces and protocols
}

uint8_t DeviceListener::Poll()
{
    if (!bPollEnable)
        return 0;

    const unsigned long &now = millis();
    if (now < qNextPollTime)
        return 0;

    for (uint8_t i = 0; i < bNumIface; i++) {
        const uint8_t& index = hidInterfaces[i].epIndex[epInterruptInIndex];
        if (index != 0) {
            uint8_t buf[constBuffLen] {0};
            uint16_t gotBytes(epInfo[index].maxPktSize);
            const uint8_t err = pUsb->inTransfer(bAddress, epInfo[index].epAddr, &gotBytes, buf);
            if (!err && gotBytes)
                onIncoming(hidInterfaces[i].bmInterface, hidInterfaces[i].bmProtocol, epInfo[index].epAddr, buf,
                           gotBytes > constBuffLen ? constBuffLen : gotBytes);
        }
    }

    qNextPollTime = now + (unsigned long)pollInterval;

    return 0;
}

// void DeviceListener::ParseHIDData(UHS_USBHID *hid, uint8_t ep, bool is_rpt_id, uint8_t len, uint8_t *data)
void DeviceListener::onIncoming(uint8_t iface, uint8_t proto, uint8_t ep, const uint8_t *data, uint16_t len)
{
    BattMonLog::log(String("onIn: ") + String(iface, HEX) + " " + String(proto, HEX) + String(" ") + String(ep, HEX));
    BattMonLog::hexes(data, len);

    /* const bool isBattStatus = */ m_battInfoHandler->parseReport(iface, proto, ep, len, data);
    /* if (!isBattStatus) */
    UsbBridge::sendHidReport(iface, proto, ep, data, len);
}

uint8_t DeviceListener::OnInitSuccessful()
{
    USB_DEVICE_DESCRIPTOR devDescr;
    uint8_t error = m_pUsb->getDevDescr(bAddress, 0, sizeof(USB_DEVICE_DESCRIPTOR), (uint8_t *)&devDescr);
    BattMonLog::log("Got dev descr:");
    BattMonLog::hexes((const uint8_t *)&devDescr, sizeof(USB_DEVICE_DESCRIPTOR));

    if (error)
        BattMonLog::log(String("Error reading device descriptor, EC: 0x") + String(error, HEX));
    else {
        if (m_battInfoHandler)
            delete m_battInfoHandler;
        m_battInfoHandler = BattInfoHandlerFactory::createHandler(PID, VID);

        UHSHIDDescrParser hidDescrParser;
        for (auto const &[iface, proto] : m_ifaceProtos) {
            hidDescrParser.m_descr.reset();
            GetReportDescr(iface, &hidDescrParser);
            hidDescrParser.m_descr.iface = iface;
            hidDescrParser.m_descr.proto = proto;

            BattMonLog::log("Got hid descr, iface #" + String(iface) + ", proto " + String(proto));
            BattMonLog::hexes(hidDescrParser.m_descr.data, hidDescrParser.m_descr.length);

            if (!UsbBridge::addHostInterface(hidDescrParser.m_descr)) {
                // TODO: restart the board
                BattMonLog::log("Host iface is NOT added");
            } else {
                BattMonLog::log("Host iface IS added as "
                                + String(hidDescrParser.m_descr.proto == USB_HID_PROTOCOL_MOUSE ? "MOUSE" : "KBD"));
                BattMonLog::hexes(hidDescrParser.m_descr.data, hidDescrParser.m_descr.length);
            }
            break; // use just the first (mouse) iface for now
        }

        String mfr, prd, srl;
        if (devDescr.iManufacturer)
            getStringDescr(devDescr.iManufacturer, &mfr);
        if (devDescr.iProduct)
            getStringDescr(devDescr.iProduct, &prd);
        if (devDescr.iSerialNumber)
            getStringDescr(devDescr.iSerialNumber, &srl);

        UsbBridge::connectToHost(&devDescr, mfr, prd, srl);
        requestBatLevel();
    }

    return error;
};

bool DeviceListener::getStringDescr(uint8_t idx, String *dst)
{
    uint8_t buf[256];

    uint8_t err = pUsb->getStrDescr(bAddress, 0, 1, 0, 0, buf);
    if (err) {
        Serial.println("Error retrieving LangID table length: 0x" + String(err, HEX));
        return false;
    }

    uint8_t length = buf[0]; // length is the first byte
    err = pUsb->getStrDescr(bAddress, 0, length, 0, 0, buf);
    if (err) {
        Serial.print("Error retrieving LangID table: 0x" + String(err, HEX));
        return false;
    }

    uint16_t langid = (buf[3] << 8) | buf[2];
    err = pUsb->getStrDescr(bAddress, 0, 1, idx, langid, buf);
    if (err) {
        Serial.print("Error retrieving string length: 0x" + String(err, HEX));
        return false;
    }
    length = buf[0];
    err = pUsb->getStrDescr(bAddress, 0, length, idx, langid, buf);
    if (err) {
        Serial.print("Error retrieving string: 0x" + String(err, HEX));
        return false;
    }

    for (int i = 2; i < length; i += 2) // string is UTF-16LE encoded
    {
        *dst += String((char)buf[i]);
    }

    return true;
}

void DeviceListener::requestBatLevel()
{
    if (m_battInfoHandler)
        m_battInfoHandler->requestInfo(this, epInfo->epAddr);
}

BatteryInfoHandlerBase::BattInfo DeviceListener::getBattInfo() const
{
    return m_battInfoHandler ? m_battInfoHandler->getBattInfo() : BatteryInfoHandlerBase::BattInfo();
}

void DeviceListener::processEvents()
{
    m_pUsb->Task();
    if (handleUsbState()) {
        if (!isPluggedIn() && m_ifaceProtos.size()) {
            m_ifaceProtos.clear();
        }
    }
}

bool DeviceListener::handleUsbState()
{
    const uint8_t usbState = m_pUsb->getUsbTaskState();
    const bool changed = usbState != m_lastUsbState;
    if (changed) {
        switch (usbState) {
        case USB_DETACHED_SUBSTATE_WAIT_FOR_DEVICE: {
            BattMonLog::log("Waiting for a mouse receiver to be plugged in.");
            break;
        }
        case USB_ATTACHED_SUBSTATE_RESET_DEVICE: {
            BattMonLog::log("Mouse connected. Resetting...");
            break;
        }
        case USB_ATTACHED_SUBSTATE_WAIT_SOF: {
            BattMonLog::log("Reset complete. Waiting for the first SOF...");
            // resetUsb();
            break;
        }
        case USB_ATTACHED_SUBSTATE_GET_DEVICE_DESCRIPTOR_SIZE: {
            BattMonLog::log("SOF generation started. Enumerating device...");
            break;
        }
        case USB_STATE_ADDRESSING: {
            BattMonLog::log("Setting device address...");
            break;
        }
        case USB_STATE_RUNNING: {
            BattMonLog::log("Plugged in and running.");
            break;
        }
        case USB_STATE_ERROR: {
            BattMonLog::log("Unknown USB error");
            break;
        }
        default: {
            break;
        }
        } // switch( usbstate...

        m_lastUsbState = usbState;
    }
    return changed;
}

bool DeviceListener::isPluggedIn() const
{
    return USB_STATE_RUNNING == m_lastUsbState;
}

bool DeviceListener::initStack()
{
    return m_pUsb->Init() == 0;
}
