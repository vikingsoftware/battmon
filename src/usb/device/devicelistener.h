#pragma once
#include "batt/batteryinfohandlerbase.h"
#include "usb/battmonhiddescr.h"

#include <Arduino.h>
#include <hidcomposite.h>
#include <map>

class HostListener;
class HIDReportParser;

class DeviceListener : public HIDComposite
{
public:
    DeviceListener(UHS_USB *pUsb);
    ~DeviceListener();

    uint8_t Poll() override;

    uint8_t GetReportDescr(uint16_t wIndex, USBReadParser *parser);
    uint8_t OnInitSuccessful() override;

    void requestBatLevel();
    bool m_showTraffic { false };

    BatteryInfoHandlerBase::BattInfo getBattInfo() const;

    void processEvents();

    bool isPluggedIn() const;

    bool initStack();

protected:
    UHS_USB *m_pUsb { nullptr };
    std::map<uint8_t, uint8_t> m_ifaceProtos;
    std::map<uint8_t, HostListener *> m_hostIfaces;
    BatteryInfoHandlerBase *m_battInfoHandler { nullptr };
    uint8_t m_lastUsbState { 0 };

    // void ParseHIDData(UHS_USBHID *hid, uint8_t ep, bool is_rpt_id, uint8_t len, uint8_t *buf) override;
    bool SelectInterface(uint8_t iface, uint8_t proto) override;

    bool getStringDescr(uint8_t idx, String *dst);
    bool handleUsbState();

    void onIncoming(uint8_t iface, uint8_t proto, uint8_t ep, const uint8_t *data, uint16_t len);
};