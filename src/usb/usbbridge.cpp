#include "usb/usbbridge.h"

#include "usb/device/devicelistener.h"
#include "usb/host/hostlistener.h"

/* static */ UsbBridge *UsbBridge::m_instance = nullptr;

UsbBridge::~UsbBridge() { }

UsbBridge::UsbBridge() { }

/* static */ UsbBridge *UsbBridge::instance()
{
    if (!m_instance)
        m_instance = new UsbBridge();
    return m_instance;
}

/* static */ void UsbBridge::processEvents()
{
    if (auto *inst = instance()) {
        inst->getDevice()->processEvents();
        inst->getHost()->processEvents();
    }
}

/* static */ bool UsbBridge::init(DeviceListener *pDevice, HostListener *pHost)
{
    if (auto *me = instance()) {
        if (pDevice && pHost) {
            me->m_pDevice = pDevice;
            me->m_pHost = pHost;
            return me->m_pDevice->initStack();
        }
    }
    return false;
}

/* static */ DeviceListener *UsbBridge::getDevice()
{
    return instance()->m_pDevice;
}

/* static */ HostListener *UsbBridge::getHost()
{
    return instance()->m_pHost;
}

/* static */ bool UsbBridge::isMousePluggedIn()
{
    return getDevice()->isPluggedIn();
}

/* static */ bool UsbBridge::addHostInterface(const BattMonHidDescr &descr)
{
    return UsbBridge::getHost()->addIface(descr);
}

/* static */ bool UsbBridge::connectToHost(const USB_DEVICE_DESCRIPTOR *info, const String &manufacturer,
                                           const String &product, const String &snumber)
{
    return UsbBridge::getHost()->start(info, manufacturer, product, snumber);
}

/* static */ bool UsbBridge::sendHidReport(uint8_t iface, uint8_t proto, uint8_t ep, const uint8_t *data, uint16_t len)
{
    return getHost()->sendData(iface, proto, ep, data, len);
}
