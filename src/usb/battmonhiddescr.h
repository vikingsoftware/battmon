#pragma once

#include <Arduino.h>

class BattMonHidDescr
{
public:
    static const uint8_t InvalidId;

    BattMonHidDescr();
    BattMonHidDescr(const BattMonHidDescr &other);
    ~BattMonHidDescr();

    uint16_t length { 0 };
    uint8_t *data { nullptr };

    uint8_t iface { InvalidId };
    uint8_t proto { InvalidId };

    void clear();
    void reset();
    void setRawDescr(const uint8_t *buf, const uint16_t len);
    void addRawDescrPart(const uint8_t *buf, const uint16_t len);

    bool operator==(const BattMonHidDescr &other) const;
};