#pragma once
#include <Usb.h>

class DeviceListener;
class HostListener;
class BattMonHidDescr;
class String;

class UsbBridge
{
public:
    ~UsbBridge();

    static UsbBridge *instance();

    static bool init(DeviceListener *pDevice, HostListener *pHost);
    static DeviceListener *getDevice();
    static HostListener *getHost();

    static bool addHostInterface(const BattMonHidDescr &descr);
    static bool connectToHost(const USB_DEVICE_DESCRIPTOR *info, const String &manufacturer, const String &product,
                              const String &snumber);

    static void processEvents();

    static bool isMousePluggedIn();

    static bool sendHidReport(uint8_t iface, uint8_t proto, uint8_t ep, const uint8_t *data, uint16_t len);

private:
    UsbBridge();
    static UsbBridge *m_instance;

    DeviceListener *m_pDevice;
    HostListener *m_pHost;
};