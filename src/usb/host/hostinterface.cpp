#include "usb/host/hostinterface.h"

#include "app/battmonlog.h"

HostInterface::HostInterface(const BattMonHidDescr &descr)
    : m_descr(descr)
    , m_hid()
{
}

HostInterface::~HostInterface()
{
    m_descr.clear();
}

uint16_t HostInterface::_onGetDescriptor(uint8_t *buffer)
{
    memcpy(buffer, m_descr.data, m_descr.length);
    return m_descr.length;
}

const BattMonHidDescr &HostInterface::getDescriptor() const
{
    return m_descr;
}

void HostInterface::begin()
{
    BattMonLog::log("Starting host iface: " + String(m_descr.iface) + " " + String(m_descr.proto));
    BattMonLog::hexes(m_descr.data, m_descr.length);

    const bool added = m_hid.addDevice(this, m_descr.length);
    BattMonLog::log("added: " + String(added));

    m_hid.begin();
}

bool HostInterface::sendReport(uint8_t iface, uint8_t proto, uint8_t ep, const uint8_t *data, uint16_t len)
{
    if (len != 7)
        return false;

    // uint8_t ok = tud_hid_n_report(1, 0, data, len);
    uint8_t ok = m_hid.SendReport(0, data, len, 0);
    BattMonLog::log("sent: " + String(ok));
    return ok;
}
