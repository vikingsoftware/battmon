#include "usb/host/hostlistener.h"

#include "app/battmonlog.h"

#include <USB.h> // espressif's USB lib
#include <usbhid.h> // UHS2.0

HostListener::HostListener()
    : m_ifaces()
    , m_started(false)
{
}

HostListener::~HostListener()
{
    m_ifaces.clear();
}

bool HostListener::addIface(const BattMonHidDescr &descr)
{
    if (m_ifaces.count(descr.iface))
        return m_ifaces[descr.iface]->getDescriptor() == descr;

    const std::shared_ptr<HostInterface> &pIfaceListener = std::make_shared<HostInterface>(descr);
    m_ifaces[descr.iface] = pIfaceListener;
    return true;
}

/* static */ void HostListener::processEvents() { }

bool HostListener::start(const USB_DEVICE_DESCRIPTOR *info, const String &manufacturer, const String &product,
                         const String &snumber)
{
    if (!m_started) {
        m_started = true;

        USB.manufacturerName(manufacturer.c_str());
        USB.productName(product.c_str());
        USB.serialNumber(snumber.c_str());

        USB.PID(info->idProduct);
        USB.VID(info->idVendor);

        USB.firmwareVersion(info->bcdDevice);
        USB.usbVersion(info->bcdUSB);
        USB.usbClass(info->bDeviceClass);
        USB.usbSubClass(info->bDeviceSubClass);
        USB.usbProtocol(info->bDeviceProtocol);
        USB.webUSB(false);
        USB.webUSBURL("");

        for (auto const &[num, listener] : m_ifaces)
            listener->begin();

        const bool ok = USB.begin();

        BattMonLog::log("USB stack started: " + String(ok));

        return true;
    }

    if (!manufacturer.equals(USB.manufacturerName()))
        return false;
    if (!product.equals(USB.productName()))
        return false;
    if (!snumber.equals(USB.serialNumber()))
        return false;

    if (USB.PID() != info->idProduct)
        return false;
    if (USB.VID() != info->idVendor)
        return false;
    if (USB.firmwareVersion() != info->bcdDevice)
        return false;
    if (USB.usbVersion() != info->bcdUSB)
        return false;
    if (USB.usbClass() != info->bDeviceClass)
        return false;
    if (USB.usbSubClass() != info->bDeviceSubClass)
        return false;
    if (USB.usbProtocol() != info->bDeviceProtocol)
        return false;

    return true;
}

bool HostListener::sendData(uint8_t iface, uint8_t proto, uint8_t ep, const uint8_t *data, uint16_t len)
{
    BattMonLog::log("onOut: " + String(iface, HEX) + " " + String(proto, HEX) + " " + String(ep, HEX));
    BattMonLog::hexes(data, len);

    /*std::map<uint8_t, std::shared_ptr<HostInterface>>::const_iterator pos = m_ifaces.find(iface);
    if (pos != m_ifaces.end())
    {
        BattMonLog::log("sending rpt as" + String(pos->second->getDescriptor().iface) + " " +
    String(pos->second->getDescriptor().proto) + " " + String(ep)); BattMonLog::hexes(data, len);

        return pos->second->sendReport(iface, proto, ep, data, len);
    }*/

    if (auto iface = m_ifaces[0]) {
        BattMonLog::log("sending rpt as" + String(iface->getDescriptor().iface) + " "
                        + String(iface->getDescriptor().proto) + " " + String(ep));
        BattMonLog::hexes(data, len);

        return iface->sendReport(0, proto, ep, data, len);
    }

    return false;
}
