#pragma once

#include "usb/battmonhiddescr.h"

#include <USBHID.h>

class HostInterface : public USBHIDDevice
{
private:
    USBHID m_hid;

    BattMonHidDescr m_descr;

public:
    HostInterface(const BattMonHidDescr &descr = BattMonHidDescr());
    ~HostInterface();

    uint16_t _onGetDescriptor(uint8_t *buffer) override;

    const BattMonHidDescr &getDescriptor() const;

    void begin();

    bool sendReport(uint8_t iface, uint8_t proto, uint8_t ep, const uint8_t *data, uint16_t len);
};