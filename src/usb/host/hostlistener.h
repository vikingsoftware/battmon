#pragma once

#include "usb/battmonhiddescr.h"
#include "usb/host/hostinterface.h"

#include <Usb.h> // UHS_2.0 lib
#include <map>
#include <memory>

class HostListener
{
private:
    std::map<uint8_t, std::shared_ptr<HostInterface>> m_ifaces;
    bool m_started;

public:
    HostListener();
    ~HostListener();

    bool addIface(const BattMonHidDescr &descr);
    bool start(const USB_DEVICE_DESCRIPTOR *info, const String &manufacturer, const String &product,
               const String &snumber);

    bool sendData(uint8_t iface, uint8_t proto, uint8_t ep, const uint8_t *data, uint16_t len);

    static void processEvents();
};