#include "usb/battmonhiddescr.h"

#include "app/battmonlog.h"

/* static */ const uint8_t BattMonHidDescr::InvalidId = 0xff;

BattMonHidDescr::BattMonHidDescr()
    : length(0)
    , data(nullptr)
    , iface(BattMonHidDescr::InvalidId)
    , proto(BattMonHidDescr::InvalidId)
{
    BattMonLog::log("BattMonHidDescr()");
}

BattMonHidDescr::BattMonHidDescr(const BattMonHidDescr &other)
    : length(other.length)
    , data(nullptr)
    , iface(other.iface)
    , proto(other.proto)
{
    BattMonLog::log("BattMonHidDescr(const BattMonHidDescr &other)");
    setRawDescr(other.data, other.length);
}

BattMonHidDescr::~BattMonHidDescr()
{
    BattMonLog::log("~BattMonHidDescr");

    clear();
}

void BattMonHidDescr::clear()
{
    BattMonLog::log("BattMonHidDescr.clear");
    delete[] data;
    data = nullptr;
    length = 0;
}

void BattMonHidDescr::reset()
{
    BattMonLog::log("BattMonHidDescr.reset");
    clear();
    iface = BattMonHidDescr::InvalidId;
    proto = BattMonHidDescr::InvalidId;
}

void BattMonHidDescr::setRawDescr(const uint8_t *buf, const uint16_t len)
{
    BattMonLog::log("BattMonHidDescr.setRaw " + String(len));

    clear();
    data = new uint8_t[len];
    std::copy(buf, buf + len, data);
    length = len;
}

void BattMonHidDescr::addRawDescrPart(const uint8_t *buf, const uint16_t len)
{
    BattMonLog::log("BattMonHidDescr.addRaw");

    uint16_t newLength = length + len;
    uint8_t *result = new uint8_t[newLength];
    if (data)
        std::copy(data, data + length, result);
    std::copy(buf, buf + len, result + length);

    if (data)
        delete[] data;

    data = result;
    length = newLength;
}

bool BattMonHidDescr::operator==(const BattMonHidDescr &other) const
{
    return length == other.length && data == other.data && iface == other.iface && proto == other.proto;
}
