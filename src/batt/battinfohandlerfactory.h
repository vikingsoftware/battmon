#pragma once

#include <Arduino.h>

class BatteryInfoHandlerBase;

class BattInfoHandlerFactory
{
public:
    static BatteryInfoHandlerBase *createHandler(uint16_t vendorId, uint16_t productId);

private:
    BattInfoHandlerFactory() = delete;
    ~BattInfoHandlerFactory() = delete;
};
