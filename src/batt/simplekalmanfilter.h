#pragma once

class SimpleKalmanFilter
{
public:
    SimpleKalmanFilter();
    ~SimpleKalmanFilter();

    double process(double newVal);

private:
    float _err_measure = 0.9;
    float _q = 0.9;
};