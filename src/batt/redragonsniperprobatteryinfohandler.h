#pragma once

#include "batt/batteryinfohandlerbase.h"

class RedragonSniperProBatteryInfoHandler : public BatteryInfoHandlerBase
{
public:
    RedragonSniperProBatteryInfoHandler();
    virtual ~RedragonSniperProBatteryInfoHandler();

    bool parseReport(uint8_t iface, uint8_t proto, uint8_t ep, uint8_t len, const uint8_t *buf) override;
    bool requestInfo(DeviceListener *hid, uint8_t ep) override;

private:
    BattInfo updateBattInfo(uint8_t level, uint8_t state, uint8_t extra) const;
};