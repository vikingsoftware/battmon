#include "batt/batterytimeestimator.h"

#include "app/battmonlog.h"

#include <cmath>

#define SECS_PER_MIN (60UL)
#define SECS_PER_HOUR (3600UL)
#define SECS_PER_DAY (SECS_PER_HOUR * 24L)

/* Useful Macros for getting elapsed time */
#define numberOfSeconds(_time_) (_time_ % SECS_PER_MIN)
#define numberOfMinutes(_time_) ((_time_ / SECS_PER_MIN) % SECS_PER_MIN)
#define numberOfHours(_time_) ((_time_ % SECS_PER_DAY) / SECS_PER_HOUR)
#define elapsedDays(_time_) (_time_ / SECS_PER_DAY)

/*static*/ BatteryTimeEstimator *BatteryTimeEstimator::m_instance = nullptr;
BatteryTimeEstimator::BatteryTimeEstimator()
{
    m_prefs.begin("battmon", false);
}

/*static*/ BatteryTimeEstimator *BatteryTimeEstimator::instance()
{
    if (!m_instance)
        m_instance = new BatteryTimeEstimator();
    return m_instance;
}

/*static*/ String BatteryTimeEstimator::updateEstimation(const BatteryInfoHandlerBase::BattInfo &info)
{
    const unsigned long now = millis() / 1000;
    const double seconds = getEstimation(info, now);

    String result;
    if (seconds > 0) {
        if (seconds > 86400) {
            const unsigned long ulSeconds(seconds);
            result = String("(" + String(elapsedDays(ulSeconds)) + "d, " + String(numberOfHours(ulSeconds)) + "h)");
        } else if (seconds > 3600)
            result = String("(" + String(seconds / 3600) + "h)");
    }
    return result;

    //
    // const int days = elapsedDays(ulSeconds);
    // const int hours = numberOfHours(ulSeconds);
    // const int minutes = numberOfMinutes(ulSeconds);
    // const int secs = numberOfSeconds(ulSeconds);

    // const bool something = (days && days <= 10) || hours || minutes || seconds;
    // auto t2display = [](const int v) { return v < 10 ? String("0") + String(v, DEC) : String(v, DEC); };
    // const String res = something
    //         ? "(" + t2display(days) + ":" + t2display(hours) + ":" + t2display(minutes) + ":" + t2display(secs) + ")"
    //         : String();

    // BattMonLog::log(String("ET: ") + res);
}

double BatteryTimeEstimator::estimateTime(unsigned long now, unsigned long prev)
{
    return m_currentInfo.state == BatteryInfoHandlerBase::BattState::Charging
            ? estimateCharging(m_currentInfo.levelPercent, now, prev)
            : estimateDischarging(m_currentInfo.levelPercent, now, prev);
}

double BatteryTimeEstimator::estimateCharging(int current, unsigned long now, unsigned long prev)
{
    const double deltaMs = double(now - prev);
    const double val = (100. - double(current)) * m_chargerFilter.process(deltaMs);

    return val;
}

double BatteryTimeEstimator::estimateDischarging(int current, unsigned long now, unsigned long prev)
{
    const double deltaMs = double(now - prev);
    const double val = double(current) * m_dischargerFilter.process(deltaMs);

    return val;
}

/* static */ double BatteryTimeEstimator::getEstimation(const BatteryInfoHandlerBase::BattInfo &info,
                                                        unsigned long nowSecs)
{
    static const double invalidRes = -1.;
    BatteryTimeEstimator *me = instance();
    if (!me)
        return invalidRes;

    if (info.state == me->m_currentInfo.state && info.level == me->m_currentInfo.level)
        return invalidRes;

    static const unsigned long FilterOutFloodIntervalMs = SECS_PER_MIN;

    if (nowSecs - me->m_lastChangeTime <= FilterOutFloodIntervalMs)
        // TODO: schedule next update to check if the status is still changed
        return invalidRes;

    switch (info.state) {
    case BatteryInfoHandlerBase::BattState::Charging:
    case BatteryInfoHandlerBase::BattState::Draining: {
        // On direction change, just save the state and timestamp
        // but do not perform the time estimation yet
        const bool canPerformEstimation =
                info.state == me->m_currentInfo.state && info.level != me->m_currentInfo.level;
        
        const unsigned long prevChangeSecs = me->m_lastChangeTime;
        me->m_currentInfo = info;
        me->m_lastChangeTime = nowSecs;

        if (canPerformEstimation)
            return me->estimateTime(nowSecs, prevChangeSecs);

        break;
    }
    default:
        break;
    }

    return invalidRes;
}