#pragma once

#include "batt/batteryinfohandlerbase.h"
#include "batt/simplekalmanfilter.h"

#include <Preferences.h>

class BatteryTimeEstimator
{
public:
    static BatteryTimeEstimator *instance();
    static void reset();

    static String updateEstimation(const BatteryInfoHandlerBase::BattInfo &info);

private:
    static BatteryTimeEstimator *m_instance;
    BatteryTimeEstimator();

    Preferences m_prefs;
    SimpleKalmanFilter m_chargerFilter;
    SimpleKalmanFilter m_dischargerFilter;

    unsigned long m_lastChangeTime { 0 };
    BatteryInfoHandlerBase::BattInfo m_currentInfo;

    double estimateTime(unsigned long now, unsigned long prev);
    double estimateCharging(int current, unsigned long now, unsigned long prev);
    double estimateDischarging(int current, unsigned long now, unsigned long prev);

    static double getEstimation(const BatteryInfoHandlerBase::BattInfo &info, unsigned long nowSecs);
};
