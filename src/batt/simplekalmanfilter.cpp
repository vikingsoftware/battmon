#include "batt/simplekalmanfilter.h"

#include <cmath>

SimpleKalmanFilter::SimpleKalmanFilter()
    : _err_measure(0.9)
    , _q(0.9)
{
}

SimpleKalmanFilter::~SimpleKalmanFilter() { }

double SimpleKalmanFilter::process(double newVal)
{
    float _kalman_gain, _current_estimate;
    static float _err_estimate = _err_measure;
    static float _last_estimate;
    _kalman_gain = (float)_err_estimate / (_err_estimate + _err_measure);
    _current_estimate = _last_estimate + (float)_kalman_gain * (newVal - _last_estimate);
    _err_estimate = (1.0 - _kalman_gain) * _err_estimate + fabs(_last_estimate - _current_estimate) * _q;
    _last_estimate = _current_estimate;
    return _current_estimate;
}