#pragma once

#include <Arduino.h>

class UHS_USBHID;
class DeviceListener;

class BatteryInfoHandlerBase
{
public:
    virtual ~BatteryInfoHandlerBase() { }

    enum BattState
    {
        Draining = 0,
        Charging,
        Charged,
        Unknown,
    };

    static BattState stateFromByte(uint8_t b);
    static String stateToString(BattState s);

    struct BattInfo {
        uint8_t level { 0 };
        uint8_t levelPercent { 0 };
        BattState state { BattState::Unknown };
        uint16_t extra { 0 };
        bool plugged { false };

        bool operator==(const BattInfo &other) const;
        bool operator!=(const BattInfo &other) const;
        String toString() const;
    };

    virtual bool parseReport(uint8_t iface, uint8_t proto, uint8_t ep, uint8_t len, const uint8_t *buf) = 0;
    virtual bool requestInfo(DeviceListener *hid, uint8_t ep) = 0;

    BattInfo getBattInfo() const;

protected:
    bool m_requestIssued { false };
    BattInfo m_battInfo;
};