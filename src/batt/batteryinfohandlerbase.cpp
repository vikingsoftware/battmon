#include "batt/batteryinfohandlerbase.h"

BatteryInfoHandlerBase::BattInfo BatteryInfoHandlerBase::getBattInfo() const
{
    return m_battInfo;
}

/* static */ BatteryInfoHandlerBase::BattState BatteryInfoHandlerBase::stateFromByte(uint8_t b)
{
    switch (b) {
    case 0:
        return BattState::Draining;
    case 1:
        return BattState::Charging;
    case 2:
        return BattState::Charged;
    default:
        return BattState::Unknown;
    }
}
/* static */ String BatteryInfoHandlerBase::stateToString(BatteryInfoHandlerBase::BattState s)
{
    switch (s) {
    case BattState::Draining:
        return "-";
    case BattState::Charging:
        return "+";
    case BattState::Charged:
        return "|";
    default:
        return "?";
    }
}

bool BatteryInfoHandlerBase::BattInfo::operator==(const BattInfo &other) const
{
    return level == other.level && state == other.state && extra == other.extra && plugged == other.plugged;
}

bool BatteryInfoHandlerBase::BattInfo::operator!=(const BattInfo &other) const
{
    return !operator==(other);
}

String BatteryInfoHandlerBase::BattInfo::toString() const
{
    const String percent = String(levelPercent, DEC);
    const String extra = String("[") + String(this->extra, HEX) + String("(") + String(this->extra, DEC) + String(")]");
    const String charging = BatteryInfoHandlerBase::stateToString(state);
    const String guiText = percent + String(" ") + extra + charging + " " + String(plugged ? "p" : "up");

    return guiText;
}