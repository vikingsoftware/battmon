#include "batt/redragonsniperprobatteryinfohandler.h"

#include "app/battmonlog.h"
#include "usb/device/devicelistener.h"

/*
ep#2
0x9, 0xa, 0x0, 0x0, 0x0, 0xa, 0x1, 0x4, 0xc1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x72 // max resolution
0x9, 0xa, 0x0, 0x0, 0x0, 0xa, 0x1, 0x3, 0xc1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x73
0x9, 0xa, 0x0, 0x0, 0x0, 0xa, 0x1, 0x2, 0xc1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x74
0x9, 0xa, 0x0, 0x0, 0x0, 0xa, 0x1, 0x1, 0xc1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x75
0x9, 0xa, 0x0, 0x0, 0x0, 0xa, 0x1, 0x0, 0xc1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x76 // min resolution

*/

RedragonSniperProBatteryInfoHandler::RedragonSniperProBatteryInfoHandler()
    : BatteryInfoHandlerBase()
{
}

RedragonSniperProBatteryInfoHandler::~RedragonSniperProBatteryInfoHandler() { }

bool RedragonSniperProBatteryInfoHandler::parseReport(uint8_t iface, uint8_t proto, uint8_t ep, uint8_t len,
                                                      const uint8_t *buf)
{
    // if (!m_requestIssued)
    // return false;

    if (iface != 1 | proto != 1 || ep != 2)
        return false;

    static const uint8_t expectedSize = 17;
    if (len != expectedSize)
        return false;

    static const uint8_t PosLevel = 6;
    static const uint8_t PosState = 7;
    static const uint8_t PosExtra = 16;
    static uint8_t expectedData[] = { 0x09, 0x04, 0x00, 0x00, 0x00, 0x02, 0xFF, 0xFF, 0x00,
                                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF };
    for (int i = 0; i < expectedSize; ++i)
        if (i != PosLevel && i != PosState && i != PosExtra)
            if (buf[i] != expectedData[i])
                return false;

    const uint8_t extra = buf[PosExtra];
    const bool full = extra == 0x46 && buf[PosLevel] == 0;
    const uint8_t level = full ? 10 : buf[PosLevel];
    const uint8_t state = full ? BatteryInfoHandlerBase::BattState::Charged : buf[PosState];
    m_battInfo = updateBattInfo(level, state, extra);

    m_requestIssued = false;

    return true;
}

BatteryInfoHandlerBase::BattInfo RedragonSniperProBatteryInfoHandler::updateBattInfo(uint8_t level, uint8_t state,
                                                                                     uint8_t extra) const
{
    return { level, uint8_t(level >= 0 ? level * 10 : 0), BatteryInfoHandlerBase::stateFromByte(state), extra, false };
}

bool RedragonSniperProBatteryInfoHandler::requestInfo(DeviceListener *hid, uint8_t ep)
{
    uint8_t data[] = { 0x08, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                       0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x49 };
    const uint16_t dataLength = 17;

    const uint8_t res = hid->SetReport(ep, 1, 3, 8, dataLength, data);
    m_requestIssued = !res;
    if (!m_requestIssued)
        BattMonLog::log(String("Req sent: 0x") + String(res, HEX));

    return m_requestIssued;
}