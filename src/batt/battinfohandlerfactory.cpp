#include "batt/battinfohandlerfactory.h"

#include "batt/redragonsniperprobatteryinfohandler.h"

/* static */ BatteryInfoHandlerBase *BattInfoHandlerFactory::createHandler(uint16_t vendorId, uint16_t productId)
{
    BatteryInfoHandlerBase *res = nullptr;

    switch (vendorId) {
    default: {
        switch (productId) {
        default: {
            res = new RedragonSniperProBatteryInfoHandler();
            break;
        }
        }
        break;
    }
    }

    return res;
}
